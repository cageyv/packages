# Python version check
PYTHON ?= python3
PYTHON_VERSION_MIN=3.6
PYTHON_VERSION=$(shell $(PYTHON) -c 'import sys; print("%d.%d"% sys.version_info[0:2])' )
PYTHON_VERSION_OK=$(shell $(PYTHON) -c 'import sys;  print(int(float("%d.%d"% sys.version_info[0:2]) >= $(PYTHON_VERSION_MIN)))' )

ifeq ($(PYTHON_VERSION_OK),0)
  $(error "Need python $(PYTHON_VERSION) >= $(PYTHON_VERSION_MIN)")
endif

# Pip args
export PIP3 ?= $(PYTHON) -m pip
export PIP3_INSTALL_ARGS ?= --user --upgrade

# Build Environment
export ARCH ?= amd64
export OS ?= $(shell uname -s | tr '[:upper:]' '[:lower:'])
export OS_ARCH ?= $(shell uname -m)
export INSTALL_PATH ?= $(HOME)/.local/bin
export CURL ?= curl --retry 3 --retry-delay 5 --fail -sSL
export PATH := $(PATH):../../bin/
export SHELL := /bin/bash

export TMP ?= /tmp

# Package details
export PACKAGE_ENABLED ?= true
export PACKAGE_NAME ?= $(notdir $(CURDIR))
export PACKAGE_REPO_NAME ?= $(PACKAGE_NAME)
export PACKAGE_EXE ?= $(PACKAGE_NAME)
export PYPI_PACKAGE_NAME ?= $(PACKAGE_NAME)
export PYPI_EXTRA_PACKAGE_NAMES ?=
export PACKAGE_DESCRIPTION ?= $(shell cat DESCRIPTION 2>/dev/null)
export PACKAGE_VERSION ?= $(shell cat VERSION 2>/dev/null)
export PACKAGE_RELEASE ?= $(shell cat RELEASE 2>/dev/null)
export PACKAGE_LICENSE ?= $(shell cat LICENSE 2>/dev/null)
export PACKAGE_HOMEPAGE_URL ?= https://github.com/$(VENDOR)/$(PACKAGE)
export PACKAGE_REPO_URL ?= https://github.com/$(VENDOR)/$(PACKAGE_REPO_NAME)

export AUTO_UPDATE_ENABLED ?= true

# Permit version to be overridden on the command line using PACKAGE_VERSION (e.g. FIGURINE_VERSION=0.3.0)
VERSION_ENV = $(shell basename $$(pwd) | tr '[:lower:]' '[:upper:]')_VERSION
ifneq ($($(VERSION_ENV)),) 
	PACKAGE_VERSION=$($(VERSION_ENV))
endif

define pip_install
	$(PIP3) install $(PIP3_INSTALL_ARGS) setuptools
	$(PIP3) install $(PIP3_INSTALL_ARGS) $(PYPI_PACKAGE_NAME)==$(PACKAGE_VERSION) $(PYPI_EXTRA_PACKAGE_NAMES)
endef

default: info

DESCRIPTION:
	@pypi-package-metadata description $(PYPI_PACKAGE_NAME) | tee DESCRIPTION

VERSION: API=releases/latest
VERSION: QUERY=.tag_name
VERSION:
	@version=$$(pypi-package-metadata latest $(PYPI_PACKAGE_NAME))
	if [ $$? -ne 0 ]; then \
		exit 1; \
	fi; \
	if [ "$${version}" == "null" -o "$${version}" == "" ]; then \
		echo "ERROR: failed to obtain current version for $(VENDOR)/$(PACKAGE_REPO_NAME)"; \
		exit 1; \
	else \
		echo "$${version}" | tee VERSION; \
	fi

LICENSE:
	@pypi-package-metadata license $(PYPI_PACKAGE_NAME) | tee LICENSE

RELEASE: VERSION LICENSE DESCRIPTION
	@if [ ! -f RELEASE ]; then \
		echo "0" | tee RELEASE; \
		git add RELEASE; \
	elif [ -n "$$(git status -s `pwd` | grep -v RELEASE)" ]; then \
		if [ -n "$$(git status -s `pwd` | grep VERSION)" ]; then \
			echo "0" | tee RELEASE; \
			git add RELEASE; \
		elif [ -z "$$(git status -s `pwd` | grep RELEASE)" ]; then \
			echo "$$(($${RELEASE}+1))" | tee RELEASE; \
			git add RELEASE; \
		fi; \
	fi

init: VERSION LICENSE DESCRIPTION RELEASE

update: CURRENT_VERSION RELEASE

sleep: RATE_LIMIT=0
sleep:
	sleep $(RATE_LIMIT)

auto-update:
	@if [ "$${AUTO_UPDATE_ENABLED:-true}" == "true" ]; then \
		$(MAKE) --quiet --silent --no-print-directory update; \
		exit $?; \
	else \
		echo "Automatic updates disabled for $${PACKAGE_NAME}"; \
		exit 0; \
	fi

# The only way to tell if VERSION is up to date is to query the source via the internet
.PHONY: CURRENT_VERSION

# Update the VERSION file only if it would change, to avoid triggering unnecessary builds
CURRENT_VERSION: API=releases/latest
CURRENT_VERSION: QUERY=.tag_name
CURRENT_VERSION::
	@local_version=$$(cat VERSION || echo 0); \
	current_version=$$(env PATH='$(PATH)' pypi-package-metadata latest $(PYPI_PACKAGE_NAME) ); \
	if [ $$? -ne 0 ]; then \
		exit 1; \
	elif [ "$${current_version}" == "null" -o -z "$${current_version}" ]; then \
		echo "ERROR: failed to obtain current version for $(VENDOR)/$(PACKAGE_REPO_NAME) (got: $${current_version})"; \
		exit 1; \
	elif [ "$${local_version}" != "$${current_version}" ]; then \
		echo "Upgrading $(PACKAGE_NAME) from $${local_version} to $${current_version}"; \
		echo "$${current_version}" > VERSION; \
	fi

info:
	@printf "%-20s %s\n" "Vendor:" "$(VENDOR)"
	@printf "%-20s %s\n" "Package:" "$(PACKAGE_NAME)"
	@printf "%-20s %s\n" "Version:" "$(PACKAGE_VERSION)"
	@printf "%-20s %s\n" "License:" "$(PACKAGE_LICENSE)"
	@printf "%-20s %s\n" "Arch:" "$(ARCH)"
	@printf "%-20s %s\n" "OS:" "$(OS)"
	@printf "%-20s %s\n" "Homepage URL:" "$(PACKAGE_HOMEPAGE_URL)"
	@printf "%-20s %s\n" "Repo URL:" "$(PACKAGE_REPO_URL)"
	@printf "%-20s %s\n" "PyPi Packages:" "$(PYPI_PACKAGE_NAMES)"
	@printf "%-20s %s\n" "Install Path:" "$(INSTALL_PATH)"

info/short:
	@printf "%-25s %-10s %s\n" "$${PACKAGE_NAME}" "$${PACKAGE_VERSION}" "$${PACKAGE_DESCRIPTION}"
